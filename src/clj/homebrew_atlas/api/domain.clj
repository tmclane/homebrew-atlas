(ns homebrew-atlas.api.domain
  (:require [schema.core :as s])
  (:import java.util.Date))

(def StatisticRange
  "Individual statistic range"
  [s/Num s/Num])

(s/defschema Location
  "Basic location"
  {:country s/Str ; (s/enum :USA)
   :city s/Str
   :state s/Str})

(s/defschema Brewer
  "Represents a brewer"
  {:name s/Str
   (s/optional-key :email) (s/maybe s/Str)
   (s/optional-key :hide_email) (s/maybe s/Bool)
   (s/optional-key :description) (s/maybe s/Str)
   :gender (s/enum :male :female :unspecified)
   :experience (s/enum :intermediate :advanced :guru :beginner)
   :comments s/Int
   :recipes s/Int
   :brews s/Int
   :timezone s/Str ; (s/enum    ; TODO: Add all timezones
   :location Location})

(s/defschema Brew
  "Represents an individual brewing of a recipe"
  {:brewer s/Str
   :recipe s/Str
   :created Date
   :modified Date
   :brewed Date
   :packaged Date
   :statistics {:og s/Num
                :fg s/Num
                :ibu s/Num
                :srm s/Num
                :abv s/Num}
   :pictures [{:value s/Str :created Date}]
   :notes [{:created Date
            :note "value"}]
   }
  )

(s/defschema MashStep
  {:duration s/Num
   :temperature s/Num
   :unit (s/enum :celsius :fahrenheit)})

(s/defschema HopAddition
  {:variety s/Str
   :alpha s/Num
   :quantity s/Num
   :unit (s/enum :ounce :gram)
   :time s/Str})

(s/defschema Fermentable
  {:category (s/enum :american :belgian :british :german :adjunct :sugar :maltextract)
   :name s/Str
   :description s/Str
   :L s/Num
   :gravity {:min s/Num :max s/Num}})

(s/defschema FermentationStep
  {:temperature s/Num
   :duration s/Num
   })

(s/defschema YeastStep
  {:temperature s/Num
   :type s/Str
   })

(s/defschema Note
  {:value s/Str
   :created Date
   :modified Date
   :author s/Str})

(s/defschema Recipe
  "Recipe schema"
  {:category (s/enum :wine :mead :beer)
   :sub-category (s/enum :extract :mini-mash :allgrain)
   :author {:email s/Str :name s/Str}
   :creator {:email s/Str :name s/Str}
   :units (s/enum :imperial :metric)
   :stats {:og s/Num
           :fg s/Num
           :ibu s/Num
           :srm s/Num
           :abv s/Num
           :batchsize s/Num
           :style s/Str
           :yeast [s/Str]}
   :fermentables [Fermentable]
   :mash [MashStep]
   :hops [HopAddition]
   :fermentation [FermentationStep]
   :yeast [YeastStep]
   :notes [Note]
  })

(s/defschema Style
  "BJCP Style"
  {:name s/Str
   :id s/Str
   :overall_impression s/Str
   :aroma s/Str
   :flavor s/Str
   :mouthfeel s/Str
   :comments s/Str
   :history s/Str
   :ingredients [s/Str]
   :comparisons [s/Str]
   :statistics {:og StatisticRange
                :fg StatisticRange
                :ibu StatisticRange
                :srm StatisticRange
                :abv StatisticRange}
   :examples [s/Str]
   :tags [s/Str]})

(def StylesResponse [Style])
