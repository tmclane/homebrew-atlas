(ns homebrew-atlas.system
  (:require [ring.middleware.defaults :refer [wrap-defaults api-defaults]]
            [ring.middleware.gzip :refer [wrap-gzip]]
            [ring.middleware.logger :refer [wrap-with-logger]]
            [environ.core :refer [env]]
            [com.stuartsierra.component :as component]
            [system.components.endpoint :refer [new-endpoint]]
            [system.components.handler :refer [new-handler]]
            [system.components.middleware :refer [new-middleware]]
            [system.components.http-kit :refer [new-web-server]]
            [homebrew-atlas.api :refer [endpoint]]
            [homebrew-atlas.components.persistence :refer [new-database]]))

(defn production-system []
  (component/system-map
   :basePath "/atlas/v1"
   :persistence (new-database)
   :routes (component/using
            (new-endpoint endpoint)
            [:basePath :persistence])
   :middleware (new-middleware  {:middleware [[wrap-defaults :defaults]
                                              wrap-with-logger
                                              wrap-gzip
                                              ]
                                 :defaults api-defaults})
   :handler (component/using
             (new-handler)
             [:routes :middleware])
   :http (component/using
          (new-web-server (Integer. (or (env :port) 10555)))
          [:handler])))

(defn development-system
  []
  (production-system))
