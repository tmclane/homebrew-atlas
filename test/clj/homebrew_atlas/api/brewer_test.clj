(ns homebrew-atlas.api.brewer-test
  (:require [cheshire.core :as cheshire]
            [clojure.test :refer :all]
            [compojure.api.sweet :as sweet]
            [ring.mock.request :as mock]
            [homebrew-atlas.components.persistence :refer [AtlasPersistence]]
            [homebrew-atlas.api.brewer :use [brewer-api]]))

(defn parse-body [body]
  (cheshire/parse-string (slurp body) true))

(defn app
  [wrapped-api components]
  (sweet/api
   (wrapped-api components)))

(def test-db
  (reify Object
    AtlasPersistence
    (brewer-profile [db _]
      {:name "brewer"
       :timezone "CST"
       :experience :beginner
       :gender :unspecified
       :recipes 0
       :brews 0
       :comments 0
       :location {:city "city"
                  :state "state"
                  :country "USA"}})))

(def components {:persistence test-db})

(deftest test-brewer-resource
  (testing "Test GET request to /brewer returns the requester's profile"
    (let [response ((app brewer-api components) (-> (mock/request :get "/brewer")))
          body     (parse-body (:body response))]
      (is (= (:status response) 200))
      (is (= (get (:headers response) "Content-Type")
             "application/json; charset=utf-8"))
      (is (= body
             {:name "brewer"
              :brews 0
              :timezone "CST"
              :comments 0
              :experience "beginner"
              :recipes 0
              :gender "unspecified"
              :location {:country "USA" :city "city" :state "state"}
              })))))
