(ns homebrew-atlas.application
  (:gen-class)
  (:require
   [com.stuartsierra.component :as component]
   [homebrew-atlas.system :refer [production-system]]))

(defn -main [& _]
  (component/start (production-system)))
