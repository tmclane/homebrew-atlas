(ns homebrew-atlas.api
  (:require [clojure.java.io :as io]
            [clojure.string :as str]
            [compojure.route :as route]
            [compojure.api.sweet :refer :all]
            [compojure.route :refer [resources]]
            [ring.util.http-response :refer :all]
            [ring.util.response :refer [response]]
            [homebrew-atlas.api.brewer :refer [brewer-api]]
            [homebrew-atlas.api.style :refer [styles-api]]
            [homebrew-atlas.api.middleware :refer [inject-headers]]
            [muuntaja.format.json :as json-format]
            [compojure.api.coercion.schema :as schema-coercion]
            ))

(def json-encoding
  {:decoder [json-format/make-json-decoder]
   :encoder [json-format/make-json-encoder]})

(defn webclient
  []
  (routes
   (GET "/" []
       (-> "public/index.html"
           io/resource
           io/input-stream
           response
           (assoc :headers {"Content-Type" "text/html; charset=utf-8"})))

   (resources "/")
   (route/not-found (slurp (io/resource "public/404.html")))))

(def no-response-coercion
  (schema-coercion/create-coercion
    (assoc schema-coercion/default-options :response nil)))

(defn atlas-api
  [{:keys [basePath] :as components}]
  (context basePath []
     (api
      {:formats {:formats {"application/json" json-encoding
                           "application/vnd.api+json" json-encoding}
                 :default-format "application/vnd.api+json"}
       :swagger
       {:spec "/api-docs/atlas.json"
        :ui "/api-docs"
        :data {:info {:title "Homebrew Atlas API"
                      :description "Helping homebrewers fashion and share homebrew recipes. The primary response format follows the JSON-API specification."
                      :version "1.0.0"}
               :tags [{:name "brewer" :description "Brewer profile related"}]
               :basePath basePath}
        :consumes ["application/vnd.api+json"]
        :produces ["application/vnd.api+json"]
        }
       :middleware [inject-headers]
       }

      (brewer-api components)
      (styles-api components))))

(defn endpoint
  [components]
  (routes
   (atlas-api components)
   (webclient)))
