(ns homebrew-atlas.components.persistence
  (:gen-class)
  (:require
   [com.stuartsierra.component :as component]))

(defn connect-to-database
  [host port]
  nil)

;; External API
(defprotocol AtlasPersistence
  "Database"
  ;; Brewer related
  (create-brewer! [db brewer] "Create a new brewer profile")
  (fetch-brewer! [db id] "Return the brewer profile specified by the 'id'")
  (update-brewer! [db id brewer] "Update the brewer record")

  ;; Styles related
  (styles [db] "Return the list of BJCP Styles")
  (style [db id] "Return an individual BJCP Style record")

  ;; Recipes related
  (recipes [db category] "Return the list of recipes in the category")

  )

(defrecord Database [host port connection]
  ;; Implement the Lifecycle protocol
  component/Lifecycle

  (start [component]
    (println ";; Starting database")
    (let [conn (connect-to-database host port)]
      (assoc component :connection conn)))

  (stop [component]
    (println ";; Stopping database")
    (when connection
      (.close connection))
    (assoc component :connection nil))

  AtlasPersistence
  (create-brewer! [db brewer]
    ; TODO: Actually store the profile
    brewer)
  (fetch-brewer! [component id]
    {:brews 0
     :timezone "CST"
     :name "Travis McLane"
     :comments 0
     :experience :guru
     :recipes 0
     :gender :unspecified
     :location {:country "USA" :city "Sugar Land" :state "TX"}})


  )

(defn new-database
  []
  (->Database nil nil nil))
