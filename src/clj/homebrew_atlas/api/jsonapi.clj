(ns homebrew-atlas.api.jsonapi
  (:require [schema.core :as s]))

(def URL s/Str)

;; JSON API Schema
(s/defschema Meta
  {:count s/Num})

(s/defschema Relationship
  {:data {:type s/Str
          :id s/Str}})

(s/defschema Relationships
  {s/Keyword Relationship})

(s/defschema LinkObject
  {:href URL
   (s/optional-key :meta) Meta})

(s/defschema Links
  {:self URL
   (s/optional-key s/Str) LinkObject})

(defn JSON-API
  "Wraps a schema element with JSON-API schema"
  [schema & meta]
  {:data {:type s/Str
          :id s/Str
          (s/optional-key :attributes) (s/maybe schema)
          (s/optional-key :relationships) (s/maybe Relationships)
          (s/optional-key :links) (s/maybe Links)
          (s/optional-key :meta) Meta
          }})
