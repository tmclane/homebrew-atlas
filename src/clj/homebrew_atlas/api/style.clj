(ns homebrew-atlas.api.style
  (:require [compojure.api.sweet :refer :all]
            [ring.util.http-response :refer :all]
            [homebrew-atlas.components.persistence :refer [styles]]
            [homebrew-atlas.api.domain :refer :all]))

(defn styles-response
  "Build a /styles response object"
  [db]
  (let [s (styles db)]
    {}))

(defn styles-api
  [components]
  (let [{db :persistence} components]
    (context "/styles" []
             :tags ["styles"]
             (GET "/" []
                  :return [Style]
                  :summary "Lists the BJCP 2015 Beer Styles"
                  (ok (styles-response db))))))
