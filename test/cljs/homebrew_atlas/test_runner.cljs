(ns homebrew-atlas.test-runner
  (:require
   [doo.runner :refer-macros [doo-tests]]
   [homebrew-atlas.core-test]
   [homebrew-atlas.common-test]))

(enable-console-print!)

(doo-tests 'homebrew-atlas.core-test
           'homebrew-atlas.common-test)
