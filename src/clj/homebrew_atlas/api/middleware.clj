(ns homebrew-atlas.api.middleware)

(def headers
  {"Server" "Homebrew Atlas 1.0"})


;; (defn wrap-user [handler]
;;   (fn [request]
;;     (if-let [user-id (-> request :session :user-id)]
;;       (let [user (get-user-by-id user-id)]
;;         (handler (assoc request :user user)))
;;       (handler request))))

(defn inject-headers [handler]
  (fn [request]
    (let [response (handler request)]
      (assoc response :headers headers))))
