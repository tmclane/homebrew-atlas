(ns homebrew-atlas.api.brewer
  (:require [clojure.string :as str]
            [compojure.api.sweet :refer :all]
            [ring.util.http-response :refer :all]
            [schema.core :as s]
            [homebrew-atlas.components.persistence :refer [fetch-brewer!
                                                           create-brewer!
                                                           update-brewer!]]
            [homebrew-atlas.api.domain :refer :all]
            [homebrew-atlas.api.jsonapi :refer [JSON-API]]))

;; Add support for 'returns'
;(defn require-role! [required roles]
;  (if-not (seq (clojure.set/intersection required roles))
                                        ;    (ring.util.http-response/unauthorized! {:text "missing role", :required required, :roles roles})))

;; Swap return specifications if necessary based on 'Accept' header
(defn update-return! [req returns]
  (let [format (:format (:muuntaja/response req))
        context (subs (:context req) 9)
        method (:req-method req)
        path [:compojure.api.request/paths :paths context :get :responses 200 :schema]
        request-spec (get-in req path)
        replacement-spec (get returns format)
        ]
 ;  (println (str "update-return!: " (:compojure.api.request/swagger req))))
    (do
;      (println "Context: " context)
;      (println "Req: " req)
;;      (println "Paths: " paths)
;      (println "Request spec: " request-spec)
;      (println "Replacement spec: " replacement-spec)
      (update-in req path replacement-spec)
      )
    ))
;    (println (str "update-return!: acc: " acc))
      ;(println (str "update-return!: " (get (:responses (:get (get paths context))) 200))))))

;(defmethod compojure.api.meta/restructure-param :returns [_ rets acc]
;  (update-in acc [:return] into ['_ `(update-return! acc)]))

(defn coerce-spec [handler returns]
  (fn [req]
    (let [format (:format (:muuntaja/response req))
          context (subs (:context req) 9)
          method (:request-method req)
          path [:compojure.api.request/paths :paths context :get :responses 200 :schema]
          request-spec (get-in req path)
          replacement-spec (get returns format)
          altered-req (assoc-in req path replacement-spec)]
      (println "Context: " context)
;      (println "Req: " req)
;;      (println "Paths: " paths)
      (println "Request spec: " request-spec)
      (println "Replacement spec: " replacement-spec)

;      (println "coerce-spec: req: " req)
      (println "coerce-spec: altered-req: " (get-in altered-req path))
      (let [response (handler (assoc-in altered-req))]
        (println "coerce-spec: response: " response)
        response))))

(defn get-accept-header
  [headers default]
  (if-let [accept (get headers "accept")]
    (first (str/split accept #";"))
    default))

(defn check-returns! [returns req]
  (let [acceptable (keys returns)
        acceptHeader (get-accept-header (:headers req) "application/vnd.api+json")]
    (if-not (contains? returns acceptHeader)
      (ring.util.http-response/not-acceptable! {:text "Accept header must be one of: "
                                                :required (keys returns)}))))

(defmethod compojure.api.meta/restructure-param :returns [_ rets acc]
;  (let [acc (assoc-in acc [:middleware]
;                       [#(coerce-spec % {"application/json" Brewer
;                                         "application/vnd.api+json" (JSON-API Brewer)})])]
                                        ;    (println "Accumulator: " acc)
    (update-in acc [:lets] into
               ['_ `(check-returns! ~rets ~'+compojure-api-request+)]))
                                        ;)

(defn require-role! [required roles]
  (if-not (seq (clojure.set/intersection required roles))
    (ring.util.http-response/unauthorized! {:text "missing role", :required required, :roles roles})))

(defmethod compojure.api.meta/restructure-param :roles [_ roles acc]
  (update-in acc [:lets] into ['_ `(require-role! ~roles (:roles ~'+compojure-api-request+))]))

(defn- wrap-jsonapi
  [brewer type id]
  {:data {:type type
          :id id
          :attributes brewer}})

(defn wrap-user [handler]
  (fn [request]
    (do
      (println (str "wrap-user:" request))
      (handler request))))

(defn brewer-response
  [request session db id]
  (let [brewer (fetch-brewer! db id)
        accept (get (:headers request) "accept")
        json-api? (or (.startsWith accept "application/vnd.api+json")
                      (.startsWith accept "*/*"))
        response  {:status 200
                   :body brewer}]
    (if json-api?
      (assoc response :body (wrap-jsonapi brewer "brewer" (str id)))
      response)))

(defn brewer-api
  [components]
  (let [{db :persistence
         session :session} components]
    (context "/brewer" []
             :tags ["brewer"]
             :returns {"application/vnd.api+json" (JSON-API Brewer)
                       "*/*" (JSON-API Brewer)}
             (GET "/" request
                  :return (JSON-API Brewer)
                  :summary "returns info about yourself"
                  (brewer-response request session db nil))
             (GET "/:id" [id :as request]
                  :return (JSON-API Brewer)
                  :summary "returns info about a brewer"
                  (brewer-response request session db id))
             (POST "/" []
                  :body [brewer (JSON-API Brewer)]
                  :summary "creates a brewer"
                  :return (JSON-API Brewer)
                  (let [brewer (create-brewer! db brewer)]
                    (created brewer)))
             (PUT "/:id" [id]
                  :body [brewer (JSON-API Brewer)]
                  :summary "updates info about a brewer"
                  (do
                    (update-brewer! db id brewer)
                    (no-content))))))
